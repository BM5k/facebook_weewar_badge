# yanked from the Dev Fu github badge by remi

helpers do
  # after a user uninstalls an app, the session becomes 
  # stale, yet Facebooker still sees it.  we need to assume 
  # that the person is *not* logged in when this happens.
  def check_session!
    begin
      session[:facebook_session].user.name if session[:facebook_session]
    rescue Facebooker::Session::SessionExpired
      session[:facebook_session] = nil
    rescue Facebooker::Session::MissingOrInvalidParameter
      session[:facebook_session] = nil # try the same thing?  I don't yet know why I get this one ...
    end
  end

  def request_from_facebook?
    params.include? 'fb_sig'
  end

  def request_from_interwebs?
    !request_from_facebook?
  end

  def authenticated_to_facebook?
    !facebook_session.nil?
  end

  # render facebook haml view (ie. don't use layout)
  def fbhaml view
    haml view, :layout => false
  end

  # facebook helpers
  def fbml
    %{<fb:fbml>\n\t#{yield}\n<fb:fbml>}
  end

  def fb_request_form options
    %Q{<fb:request-form
        action="#{options[:action]}"
        content="#{options[:content]}"
        invite="#{options[:invite]}"
        method="#{options[:method]}"
        type="#{options[:type]}">
      #{yield}
      </fb:request-form>
    }
  end

  def fb_invite_options content = '', label = nil
    options = {
     :action  => "#{FB_URL}invite",
     :content => "#{content} #{Sanitize.encode_html fb_req_choice(label)}",
     :invite  => true,
     :method  => 'POST',
     :type    => 'Weewar Badge'
    }
  end

  def fb_req_choice label
    label ||= 'Authorize My Application'
    %{<fb:req-choice url="#{FB_URL}" label="#{label}" />}
  end

  def fb_multi_friend text, border = false
    %{<fb:multi-friend-selector showborder="#{border}" actiontext="#{text}" />}
  end

  def fb_profile_pic id
    "<fb:profile-pic uid='#{id}' />"
  end

  def fb_user_name id
    "<fb:name uid='#{id}' />"
  end

  def fb_add_section section
    %{<fb:add-section-button section="#{section}" />}
  end
end
