class User < ActiveRecord::Base
  before_save :update_from_api

  validates_presence_of :facebook_id
  validates_presence_of :weewar_name

  def request_url
    "http://weewar.com/api1/user/#{weewar_name}"
  end

  def game_record
    @game_record ||= "#{victories}W - #{losses}L - #{draws}D"
  end

private

  def api_data
    begin
      xml  = open(request_url).read
      Hash.from_xml(xml)['user']
    rescue OpenURI::HTTPError
      {} # api error
    end
  end

  def update_from_api
    player = api_data

    self.points    = player['points']
    self.losses    = player['losses']
    self.draws     = player['draws']
    self.victories = player['victories']
    true
  end

  def update_from_api!
    update_from_api
    save!
  end
end

class CreateUsers < ActiveRecord::Migration
  def self.up
    create_table :users, :force => true do |t|
      t.string  :facebook_id, :null => false # too long to be an integer
      t.string  :weewar_name, :null => false
      t.integer :points
      t.integer :victories
      t.integer :draws
      t.integer :losses
      t.timestamps
    end

    add_index :users, :facebook_id, :unique => true
    add_index :users, :weewar_name
  end
end

CreateUsers.migrate :up unless ActiveRecord::Base.connection.tables.include?('users')