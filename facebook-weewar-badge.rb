#! /usr/bin/env ruby
%w( rubygems sinatra frankie activerecord open-uri haml sass htmlentities sanitize ).each {|lib| require lib }
require 'htmlentities'
require 'sanitize'

FB_URL  = 'http://apps.facebook.com/weewar-badge/'
PUB_URL = 'http://facebook-weewar-badge.heroku.com'

use Rack::Session::Cookie

configure do
  ENV['RACK_ENV'] ||= 'development'
  load_facebook_config("./config/facebooker.yml", ENV['RACK_ENV']) unless ENV['FACEBOOK_ENV'] == 'true'
  ActiveRecord::Base.establish_connection YAML.load(File.read('config/database.yml'))[ ENV['RACK_ENV'] ]
end

require File.dirname(__FILE__) + '/user'
require File.dirname(__FILE__) + '/helpers'

error do
  raise env['sinatra.error'] # bubble it up!  i want it!
end

before do
  check_session!
end

post '/invite/new' do
  fbhaml :invite
end

post '/invite/?' do
  @recipients = params[:ids] || []
  fbhaml :invited
end

post '/associate_facebook_user_with_weewar_user' do
  user = User.find_or_create_by_facebook_id params['facebook_id']
  user.update_attribute :weewar_name, params['weewar_name']

  redirect FB_URL
end

get '/stylesheets/:name.css' do
  content_type 'text/css', :charset => 'utf-8'
  sass :"stylesheets/#{params[:name]}", :style => :compact, :load_paths => [File.join(Sinatra::Application.views, 'stylesheets')]
end

# catch all
%w( get post put delete ).each do |method|
  send(method, /.*/, &lambda {

    if params['installed'] # we have been redirected here from the installation process, redirect back!
      redirect FB_URL

    elsif request_from_interwebs?
      haml :public_index

    else
      ensure_authenticated_to_facebook
      ensure_application_is_installed_by_facebook_user

      @user = User.find_by_facebook_id facebook_session.user.id.to_s

      if @user
        facebook_session.user.profile_main = fbhaml(:badge)
        fbhaml :existing_user
      else
        fbhaml :new_user
      end
    end
  })
end

