require 'facebook-weewar-badge'

use Rack::ShowExceptions
use Rack::CommonLogger

set :root, File.dirname(__FILE__)

run Sinatra::Application
